package com.elicense.payment.paytm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaytmController {
	
	@Autowired
	PaytmService paytmService;
	
	@RequestMapping("redirectToPaytm")
	public void redirectToPaytm(HttpServletRequest request,HttpServletResponse response) throws Exception {
		paytmService.goToPaytm(request,response);
	}
	
	@RequestMapping("paytmResponse")
	public String paytmResponse(HttpServletRequest request,HttpServletResponse response) throws Exception {
		return paytmService.comeBack(request,response);
	}
	

}
