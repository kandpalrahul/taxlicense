package com.elicense.payment.paytm;

import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.paytm.pg.merchant.CheckSumServiceHelper;

@Service
public class PaytmService {
	
public void goToPaytm(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		Enumeration<String> paramNames = request.getParameterNames();
		Map<String, String[]> mapData = request.getParameterMap();
		TreeMap<String,String> parameters = new TreeMap<String,String>();
		String paytmChecksum =  "";
		while(paramNames.hasMoreElements()) {
			//String paramName = (String)paramNames.nextElement();
			//parameters.put(paramName,mapData.get(paramName)[0]);
			parameters.put("username","USER4");
			parameters.put("TXN_AMOUNT","50");
			
		}
		parameters.put("MID",PaytmConstants.MID);
		parameters.put("CHANNEL_ID",PaytmConstants.CHANNEL_ID);
		parameters.put("INDUSTRY_TYPE_ID",PaytmConstants.INDUSTRY_TYPE_ID);
		parameters.put("WEBSITE",PaytmConstants.WEBSITE);
		parameters.put("MOBILE_NO","9716807003");
		parameters.put("EMAIL","rkandpal.47@gmail.com");
		parameters.put("CALLBACK_URL", "http://localhost:8080/paytmResponse");
		String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(PaytmConstants.MERCHANT_KEY, parameters);
		
		StringBuilder outputHtml = new StringBuilder();
		outputHtml.append("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
		outputHtml.append("<html>");
		outputHtml.append("<head>");
		outputHtml.append("<title>Merchant Check Out Page</title>");
		outputHtml.append("</head>");
		outputHtml.append("<body>");
		outputHtml.append("<center><h1>Please do not refresh this page...</h1></center>");
		outputHtml.append("<form method='post' action='"+ PaytmConstants.PAYTM_URL +"' name='f1'>");
		outputHtml.append("<table border='1'>");
		outputHtml.append("<tbody>");
		for(Map.Entry<String,String> entry : parameters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			outputHtml.append("<input type='hidden' name='"+key+"' value='" +value+"'>");	
		}	  
			  
		outputHtml.append("<input type='hidden' name='CHECKSUMHASH' value='"+checkSum+"'>");
		outputHtml.append("</tbody>");
		outputHtml.append("</table>");
		outputHtml.append("<script type='text/javascript'>");
		outputHtml.append("document.f1.submit();");
		outputHtml.append("</script>");
		outputHtml.append("</form>");
		outputHtml.append("</body>");
		outputHtml.append("</html>");
		response.setContentType("text/html;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(outputHtml.toString());
		
	}

public String comeBack(HttpServletRequest request, HttpServletResponse response) {
	Enumeration<String> paramNames = request.getParameterNames();
	Map<String, String[]> mapData = request.getParameterMap();
	TreeMap<String,String> parameters = new TreeMap<String,String>();
	String paytmChecksum =  "";
	while(paramNames.hasMoreElements()) {
		String paramName = (String)paramNames.nextElement();
		if(paramName.equals("CHECKSUMHASH")){
			paytmChecksum = mapData.get(paramName)[0];
		}else{
			parameters.put(paramName,mapData.get(paramName)[0]);
		}
	}
	boolean isValideChecksum = false;
	String outputHTML="";
	try{
		isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(PaytmConstants.MERCHANT_KEY,parameters,paytmChecksum);
		if(isValideChecksum && parameters.containsKey("RESPCODE")){
			if(parameters.get("RESPCODE").equals("01")){
				outputHTML = parameters.toString();
			}else{
				outputHTML="<b>Payment Failed.</b>";
			}
		}else{
			outputHTML="<b>Checksum mismatched.</b>";
		}
	}catch(Exception e){
		outputHTML=e.toString();
	}
	return outputHTML;
}

}
