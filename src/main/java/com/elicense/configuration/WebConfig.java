package com.elicense.configuration;

import java.util.Collection;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	/**
	 * redirect a user to the welcome page when he visits tha app without a
	 * destination url.
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
				registry.addViewController("/login").setViewName("forward:/login.html");
		        registry.addViewController("/").setViewName("redirect:/home");
        		registry.addViewController("/home").setViewName("forward:/Home.html");
            
            	//registry.addViewController("/").setViewName("redirect:/admin/home");
        		registry.addViewController("/admin/home").setViewName("forward:/admin/Home.html");
        		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        		super.addViewControllers(registry);
	}

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/*.js/**").addResourceLocations("/ui/static/");
		registry.addResourceHandler("/*.css/**").addResourceLocations("/ui/static/");
	}
}
