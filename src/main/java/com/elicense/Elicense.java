package com.elicense;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class Elicense extends SpringBootServletInitializer {

		// TODO Auto-generated method stub
		@Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
			return application.sources(Elicense.class);
		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			SpringApplication.run(Elicense.class, args);
		}
}


