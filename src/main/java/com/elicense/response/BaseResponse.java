package com.elicense.response;

import org.springframework.stereotype.Component;

@Component
public class BaseResponse {

	public static final String SUCCESS_STATUS = "success";
	public static final String ERROR_STATUS = "error";
	public static final int CODE_SUCCESS = 100;
	public static final int AUTH_FAILURE = 102;
	
	private String status;
	private Integer code;
 
	public String getStatus() {
		return status;
	}
 
	public void setStatus(String status) {
		this.status = status;
	}
 
	public Integer getCode() {
		return code;
	}
 
	public void setCode(Integer code) {
		this.code = code;
	}
}
