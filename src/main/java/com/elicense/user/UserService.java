package com.elicense.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	
	public int addUser(User u) {
		return (int) userRepository.save(u).getId();
	}
	
	public void addLoginDetails(String username,String password,int enabled,String role)
	{
		userRepository.addLoginDetails(username, password, enabled,role);
	}
	public User getUserDetails(String username)
	{
		return userRepository.findByUsername(username);
	}
	public void addUsername(String username,int id)
	{
		userRepository.addUsername(username, id);
	}
	public void updateDetails(String firstName,String lastName,String fatherName,String email,int mobile,String address,
			String pic,String id,String username)
	{
		userRepository.updateDetails(firstName,lastName,fatherName,email,mobile,address,pic,id,username);
	}
	
	public List<User> getAllUsersByStatus(String status) {
		List<User> users = new ArrayList<>();
		userRepository.findAllByStatus(status).forEach(users::add);
		return users;
	}
	public int updateStatus(int id) {
		return userRepository.updateStatus((long) id);
	}

	public List<User> getAllUsers(String key, String value) {
		List<User> users = new ArrayList<>();
		if ("NAME".equals(key))
			userRepository.findAllByNameLikeOrderByName(value).forEach(users::add);
		else if ("FATHER_NAME".equals(key))
			userRepository.findAllByFatherName(value).forEach(users::add);
		else {
			long l = Long.parseLong(value);
			userRepository.findAllById(l).forEach(users::add);
		}
		return users;
	}
/*
	public User getUser(int id) {
		return userRepository.findById(id);
	}

	public int updatePaths(String p1, String p2, int id) {
		return userRepository.updatePath(p1, p2, (long) id);
	}

	
*/
}
