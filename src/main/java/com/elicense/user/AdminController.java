package com.elicense.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.etax.register.User;
//import com.etax.register.UserService;
import com.elicense.response.BaseResponse;
import com.elicense.user.User;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private BaseResponse response;
	
	@RequestMapping(value = "/getUsers/{val}")
	public List<User> getAllUsers(@PathVariable("val") String status) {
		return userService.getAllUsersByStatus(status);
	}
	
	@RequestMapping(value = "/approve/{id}")
	public void approveUser(@PathVariable("id") int id)
	{
		userService.updateStatus(id);
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<User> getAllUser(@RequestParam("name") String value, @RequestParam("key") String key) {
		return userService.getAllUsers(key, value);
	}
}
