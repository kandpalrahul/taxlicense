package com.elicense.user;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.elicense.user.User;
import com.elicense.user.UserService;
import com.elicense.response.BaseResponse;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private BaseResponse response;
	private int userId = 0;
	private String picPath = "";
	private String idProofPath = "";
	private String loggedinusername;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public BaseResponse checkLogin(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("email") String email, @RequestParam("mobile") long mobile,@RequestParam("password") String password, 
			HttpServletRequest servletRequest) {
	
		User u = new User();
		LoginDetails l = new LoginDetails();

		u.setFirstName(firstName);
		u.setLastName(lastName);
		u.setEmail(email);
		u.setMobile(mobile);
		u.setStatus("NEW");
		u.setRecordDate();
		// u.setPic(picFile.getBytes());
		// u.setIdProof(idProof.getBytes());
		userId = userService.addUser(u);		

		if (userId > 0) {			
			userService.addLoginDetails(("USER"+userId),password,1,"USER");
			userService.addUsername(("USER"+userId), userId);
			response.setStatus(BaseResponse.SUCCESS_STATUS);
			response.setCode(BaseResponse.CODE_SUCCESS);
		} else {
			response.setStatus(BaseResponse.ERROR_STATUS);
			response.setCode(BaseResponse.AUTH_FAILURE);
		}
		return response;
	}
    
	@RequestMapping(value = "/getUser")
	public User getUserDetails() {
		//User loggedInUser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 loggedinusername = auth.getName();
		//System.out.println("**************************"+name);
		return userService.getUserDetails(loggedinusername);
	}
	
	@RequestMapping(value = "/addDetails", method = RequestMethod.POST)
	public BaseResponse checkLogin(@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,
			@RequestParam("fatherName") String fatherName,@RequestParam("email") String email, @RequestParam("mobile") int mobile,
			@RequestParam("address") String address, @RequestParam("picFile") MultipartFile picFile,
			@RequestParam("idProof") MultipartFile idProof, HttpServletRequest servletRequest) {
		
		
		picPath = servletRequest.getServletContext().getRealPath("/UserPics/");
		idProofPath = servletRequest.getServletContext().getRealPath("/UserIdProof/");

		java.io.File f = new java.io.File(picPath);
		if (!f.exists()) {
			f.mkdirs();
		}
		java.io.File f2 = new java.io.File(idProofPath);
		if (!f2.exists()) {
			f2.mkdirs();
		}

		if (loggedinusername!=null) {
			Path path1 = Paths.get(picPath + userId + picFile.getOriginalFilename()
					.substring(picFile.getOriginalFilename().lastIndexOf("."), picFile.getOriginalFilename().length()));
			Path path2 = Paths.get(idProofPath + userId + idProof.getOriginalFilename()
					.substring(idProof.getOriginalFilename().lastIndexOf("."), idProof.getOriginalFilename().length()));
			try {
				Files.write(path1, picFile.getBytes());
				Files.write(path2, idProof.getBytes());
			} catch (Exception e) {
			}
			
			String pic = path1.toString().substring(path1.toString().lastIndexOf("UserPics"), path1.toString().length());
			String id = path2.toString().substring(path2.toString().lastIndexOf("UserIdProof"), path2.toString().length());
			
			userService.updateDetails(firstName,lastName,fatherName,email,mobile,address,pic,id,loggedinusername);
			
			response.setStatus(BaseResponse.SUCCESS_STATUS);
			response.setCode(BaseResponse.CODE_SUCCESS);
		} else {
			response.setStatus(BaseResponse.ERROR_STATUS);
			response.setCode(BaseResponse.AUTH_FAILURE);
		}
		return response;
	}
}
