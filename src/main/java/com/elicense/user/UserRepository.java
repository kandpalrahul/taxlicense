package com.elicense.user;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends CrudRepository<User,Integer> {

	@Modifying
	@Transactional
	@Query(value="insert into login_details(username,password,enabled,role) values(?1,?2,?3,?4)",nativeQuery=true)
	public void addLoginDetails(String username,String password,int id,String role);
	@Modifying
	@Transactional
	@Query(value="update user set username=?1 where id=?2",nativeQuery=true)
	public void addUsername(String username,int id);
	
	public User findByUsername(String username);
	
	public List<User> findAllByStatus(String status);

	
	@Modifying
	@Transactional
	@Query(value="update user set first_name=?1,last_name=?2,father_name=?3,email=?4,mobile=?5,address=?6,"
			+ "pic_path=?7,id_proof_path=?8,status='NOT-APPROVED' where username=?9",nativeQuery=true)
	public void updateDetails(String firstName,String lastName,String fatherName,String email,int mobile,String address,
			String pic,String id,String username);
	
	@Modifying
	@Transactional
	@Query("update User u set u.status='APPROVED' where u.id=?1")
	public int updateStatus(long id);
	
	@Query("select u from User u where u.firstName like %?1%")
	public List<User> findAllByNameLikeOrderByName(String name);

	@Query("select u from User u where u.fatherName like %?1%")
	public List<User> findAllByFatherName(String fatherName);
	
	@Query("select u from User u where u.id=?1")
	public List<User> findAllById(long id);
	/*
	
	
	@Query("select u from User u where u.id=?1")
	public User findById(long id);

	@Modifying
	@Transactional
	@Query("update User u set u.picPath=?1,u.idProofPath=?2 where u.id=?3")
	public int updatePath(String path1,String path2,long id);
	
	*/
}
