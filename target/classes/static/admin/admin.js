
// Get All Users
function getAllUsers(status){
	    $.get("/admin/getUsers/"+status, function(data, status){
	    	if(status=="success")
	        {	$("form").remove();
	        	$("#pageHeading").text("Registeration List For License");
	        	var cnt=0;
	        	var result = JSON.parse(JSON.stringify(data));
				var text="<br/><table class='table table-bordered'><tr><th>SNO</th><th>Photo</th><th>User ID</th>"+
				"<th>First Name</th><th>Last Name</th><th>Father Name</th><th>Mobile</th><th>Email</th>"+
				"<th>Address</th><th>ID Proof</th></tr>";
					if(status=='NEW')	
						{
						    $.each(result,function(id,obj){
						
							text+="<tr><td>"+(++cnt)+"</td>";
							text+="<td align='center'>Photo Not Uploaded</td>";	
							text+="<td>"+obj.id+"</td>";
							text+="<td>"+obj.firstName+"</td>";
							text+="<td>"+obj.lastName+"</td>";
							text+="<td>"+obj.fatherName+"</td>";
							text+="<td>"+obj.mobile+"</td>";
							text+="<td>"+obj.email+"</td>";
							text+="<td>"+obj.address+"</td>";
							text+="<td align='center'>Document Not Uploaded</td>";
							//text+="<td><a href='approve/"+obj.id+"'>APPROVE</a></td></tr>";
						});
						text+="</table>";
	        		}
					else
						{
							$.each(result,function(id,obj){
							
							text+="<tr><td>"+(++cnt)+"</td>";
							text+="<td align='center'><img src=../"+obj.picPath+" height='100' width='100' alt='Not Uploaded'/></td>";
							text+="<td>"+obj.id+"</td>";
							text+="<td>"+obj.firstName+"</td>";
							text+="<td>"+obj.lastName+"</td>";
							text+="<td>"+obj.fatherName+"</td>";
							text+="<td>"+obj.mobile+"</td>";
							text+="<td>"+obj.email+"</td>";
							text+="<td>"+obj.address+"</td>";
							text+="<td align='center'><a href=../"+obj.idProofPath+" target='_blank'>ID Proof</a></td>";
							text+="<td><a href='#' onclick='approveUser("+obj.id+")'>APPROVE</a></td></tr>";
						});
						text+="</table>"
					 }
            	document.getElementById("main-content").innerHTML=text;	
	        }
	    });
	};
//
	function approveUser(id){
	    $.get("/admin/approve/"+id, function(data, status){
	    	if(status=="success")
	        {	
	    		getAllUsers("APPROVE");
	        }
	    });
	} 
	
// Search User By Name	
	function search()
	{
		   $.post("/admin/search",
			        {
		          		name: $("#name").val(),
		          		key: $("#key").val()
			        },
			        function(data,status){
			            if(status=="success")
			            	{
			            	
			            	var result = JSON.parse(JSON.stringify(data));
							var text="<br/><table class='table table-bordered'><tr><td colspan='6'><center><b>Search Result</b></center></td></tr><tr><th>ID</th>"+
							"<th>Name</th><th>Father Name</th><th>Mobile</th><th>Email</th><th>Address</th></tr>";
								$.each(result,function(id,obj){
									text+="<tr><td>"+obj.id+"</td>";
									text+="<td>"+obj.name+"</td>";
									text+="<td>"+obj.fatherName+"</td>";
									text+="<td>"+obj.mobile+"</td>";
									text+="<td>"+obj.email+"</td>";
									text+="<td>"+obj.address+"</td></tr>";
								});
								text+="</table>"
			            	document.getElementById("result").innerHTML=text;	
			            	}
			            
			        });
		}