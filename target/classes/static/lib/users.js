 function getUserDetails()
 {
	 $.get("getUser", function(data, status){
		 if(status=="success")
	        {
	    		var result = JSON.parse(JSON.stringify(data));
	    		if(result.status=='NOT-APPROVED')
	    		{
	    			$("#pageHeading").text('Your Details');
	    			$('#picFile').parent().parent().remove();
	    			$('#idProof').parent().parent().remove();
					$('.btn').remove();
	    		}
	    		$("#firstName").val(result.firstName);
	    		$("#lastName").val(result.lastName);
	    		$("#fatherName").val(result.fatherName);
	    		$("#email").val(result.email);
	    		$("#mobile").val(result.mobile);
	    		$("#address").val(result.address);
	        }
	 });
 }